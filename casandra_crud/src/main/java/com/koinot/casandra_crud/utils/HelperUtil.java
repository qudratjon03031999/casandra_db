package com.koinot.casandra_crud.utils;

import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.model.Address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;

public class HelperUtil {

    private HelperUtil() {
    }

    public static List<Users> getHumanesData() {
        List<Users> users=new ArrayList<>();
        System.out.println("begin "+new Date());
        for (int i = 0; i < 1000000; i++) {
            users.add(
                    Users.builder().id(1L+i).name("Qudratjon"+i).profession("backend-developer"+i).age(23+i)
                    .address(Address.builder().addressname("shayxontohur"+i).beacon("ko'kcha masjit"+i).build()).build()
            );
        }
        System.out.println("ready data "+new Date());

//        return HumanesSupplier.get();
        return users;
    }

    private static final Supplier<List<Users>> HumanesSupplier = () ->
            Arrays.asList(
                    Users.builder().id(1L).name("Qudratjon").profession("backend-developer").age(23)
                            .address(Address.builder().addressname("shayxontohur").beacon("ko'kcha masjit").build()).build(),

                    Users.builder().id(2L).name("Assomiddin").profession("backend-developer").age(45)
                            .address(Address.builder().addressname("Toshkent").beacon("oybek metro").build()).build(),

                    Users.builder().id(3L).name("Baxtiyor").profession("backend-developer").age(21)
                            .address(Address.builder().addressname("Toshkent").beacon("oybek metro").build()).build()
            );
}