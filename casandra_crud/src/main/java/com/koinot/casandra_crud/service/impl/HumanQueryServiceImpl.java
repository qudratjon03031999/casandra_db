package com.koinot.casandra_crud.service.impl;

import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.repository.HumanQueryRepository;
import com.koinot.casandra_crud.service.HumanQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HumanQueryServiceImpl implements HumanQueryService {

    @Autowired
    private HumanQueryRepository HumanQueryRepository;

    @Override
    public List<Users> save() {
        return HumanQueryRepository.save();
    }

    @Override
    public List<Users> getAll() {
        return HumanQueryRepository.getAll();
    }

    @Override
    public List<Users> getHumanByName(String name) {
        return HumanQueryRepository.getHumanByName(name);
    }

    @Override
    public Users getOneHumanByName(String name) {
        return HumanQueryRepository.getOneHumanByName(name);
    }

    @Override
    public List<Users> getHumanByNameLike(String name) {
        return HumanQueryRepository.getHumanByNameLike(name);
    }

    @Override
    public List<Users> getHumanByAgeGreaterThan(int age) {
        return HumanQueryRepository.getHumanByAgeGreaterThan(age);
    }

}
