package com.koinot.casandra_crud.service.impl;

import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.repository.HumanRepository;
import com.koinot.casandra_crud.service.HumanService;
import com.koinot.casandra_crud.utils.HelperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HumanServiceImpl implements HumanService {

    @Autowired
    private HumanRepository repository;

    @Override
    public List<Users> save() {

        List<Users> users = repository.findAll();
        if (users.isEmpty()) {
            System.out.println("get data "+new Date());

            repository.saveAll(HelperUtil.getHumanesData());

            System.out.println("save data "+new Date());

        }

        return repository.findAll();
    }

    @Override
    public List<Users> findAll() {
        return repository.findAll();
    }

    @Override
    public Users findById(Long id) {
        return repository.findById(id).orElse(Users.builder().build());
    }

    @Override
    public Users save(Users users) {
        return repository.save(users);
    }

    @Override
    public Users update(Users users) {
        return repository.save(users);
    }

    @Override
    public void delete(Long id) {
        repository.findById(id).ifPresent(Human -> repository.delete(Human));
    }
}