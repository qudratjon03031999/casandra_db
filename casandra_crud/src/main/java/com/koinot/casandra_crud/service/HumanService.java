package com.koinot.casandra_crud.service;


import com.koinot.casandra_crud.model.Users;

import java.util.List;

public interface HumanService {

    List<Users> save();

    List<Users> findAll();

    Users findById(Long id);

    Users save(Users users);

    Users update(Users users);

    void delete(Long id);

}