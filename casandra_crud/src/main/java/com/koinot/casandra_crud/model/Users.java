package com.koinot.casandra_crud.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Data
@Builder
@Table("users")
public class Users implements Serializable {

    @PrimaryKey
    private Long id;

    private String name;

    private String profession;

    private int age;

    @Column("address")
    private Address address;

}