package com.koinot.casandra_crud.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

import java.io.Serializable;

@Data
@Builder
@UserDefinedType("address")
public class Address implements Serializable {
    private String addressname;

    private String beacon;
}
