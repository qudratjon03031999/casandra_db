package com.koinot.casandra_crud.repository;

import com.koinot.casandra_crud.model.Users;

import java.util.List;

public interface HumanQueryRepository {

    List<Users> save();

    List<Users> getAll();

    List<Users> getHumanByName(String name);

    Users getOneHumanByName(String name);

    List<Users> getHumanByNameLike(String name);


    List<Users> getHumanByAgeGreaterThan(int age);


}
