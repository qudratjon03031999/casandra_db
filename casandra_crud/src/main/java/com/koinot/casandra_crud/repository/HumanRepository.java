package com.koinot.casandra_crud.repository;

import com.koinot.casandra_crud.model.Users;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumanRepository extends CassandraRepository<Users, Long> {
}
