package com.koinot.casandra_crud.repository.impl;

import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.repository.HumanQueryRepository;
import com.koinot.casandra_crud.utils.HelperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class HumanQueryRepositoryImpl implements HumanQueryRepository {

    @Autowired
    private CassandraOperations cassandraTemplate;

    @Override
    public List<Users> save() {
        List<Users> users = cassandraTemplate.select(Query.empty(), Users.class);
        if (users.isEmpty())
            cassandraTemplate.insert(HelperUtil.getHumanesData());

        return cassandraTemplate.select(Query.empty(), Users.class);
    }

    @Override
    public List<Users> getAll() {
        return cassandraTemplate.select(Query.empty(), Users.class);
    }

    @Override
    public List<Users> getHumanByName(String name) {
        return cassandraTemplate.select(Query.query(Criteria.where("name").is(name)).withAllowFiltering(), Users.class);
    }

    @Override
    public Users getOneHumanByName(String name) {
        return cassandraTemplate.selectOne(Query.query(Criteria.where("name").is(name)).withAllowFiltering(), Users.class);
    }

    @Override
    public List<Users> getHumanByNameLike(String name) {
        return cassandraTemplate.select(Query.query(Criteria.where("name").like(name)).withAllowFiltering(), Users.class);
    }


    @Override
    public List<Users> getHumanByAgeGreaterThan(int age) {
        return cassandraTemplate.select(Query.query(Criteria.where("age").gt(age)).withAllowFiltering(), Users.class);
    }


}
