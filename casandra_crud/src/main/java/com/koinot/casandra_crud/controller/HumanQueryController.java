package com.koinot.casandra_crud.controller;


import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.service.HumanQueryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/humas-query")
@Tag(name = "Human Query controller", description = "Get Human APIs using Queries")
public class HumanQueryController {

    @Autowired
    private HumanQueryService HumanQueryService;


    @Operation(summary = "Get all Humanes using query")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humanes list",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Users.class)))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping
    public List<Users> getAll() {
        return HumanQueryService.getAll();
    }


    @Operation(summary = "Get all Humanes by name using query")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humanes list",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Users.class)))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/name/{name}")
    public List<Users> getHumanByName(@Parameter(description = "Human name to be fetched") @PathVariable String name) {
        return HumanQueryService.getHumanByName(name);
    }


    @Operation(summary = "Get one Human by name using query")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get one Human by name",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Users.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/one-by-name/{name}")
    public Users getOneHumanByName(@Parameter(description = "Human name to be fetched") @PathVariable String name) {
        return HumanQueryService.getOneHumanByName(name);
    }

    @Operation(summary = "Get one Human by name like using query (Only on indexed columns)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get one Human by name like",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Users.class)))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/name-like/{name}")
    public List<Users> getHumanByNameLike(@Parameter(description = "Human name to be fetched") @PathVariable String name) {
        return HumanQueryService.getHumanByNameLike(name);
    }


    @Operation(summary = "Get all Humanes whose age greater than using query")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humanes list",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Users.class)))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/age-greater-than/{age}")
    public List<Users> getHumanByAgeGreaterThan(@Parameter(description = "Humanes fetched whose age greater than") @PathVariable int age) {
        return HumanQueryService.getHumanByAgeGreaterThan(age);
    }


}
