package com.koinot.casandra_crud.controller;


import com.koinot.casandra_crud.model.Users;
import com.koinot.casandra_crud.service.HumanService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/humas")
@Tag(name = "Human JPA controller", description = "Human CRUD API with documentation annotations")
public class HumanController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HumanService HumanService;


    @Operation(summary = "Save dummy Humanes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Saved Humanes list"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/save")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void save() {

        logger.info("*** Storing dummy static data to DB ***");
        List<Users> list = HumanService.save();
//        logger.info("Stored data to DB :: {}", list);
    }


    @Operation(summary = "Get all Humanes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Humanes list",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Users.class)))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<Users>> findAll() {

        logger.info("*** Getting Humanes from DB ***");
        List<Users> list = HumanService.findAll();
        logger.info("Humanes fetched from DB :: {}", list);

        return ResponseEntity.ok().body(list);
    }




    @Operation(summary = "Get a Human by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Human",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Users.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Human not found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<Users> findById(@PathVariable Long id) {

        logger.info("*** Getting Human from DB for Id :: {}", id);
        Users users = HumanService.findById(id);

        if (StringUtils.isEmpty(users.getName()))
            return ResponseEntity.notFound().build();

        logger.info("Human fetched :: {}", users);
        return ResponseEntity.ok().body(users);
    }




    @Operation(summary = "Create Human ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Save Human",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Users.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @PostMapping
    public ResponseEntity<Users> save(@Parameter(description = "Human object to be created") @RequestBody Users users) {

        logger.info("*** Saving Human to DB :: {}", users);
        Users savedUsers = HumanService.save(users);
        logger.info("*** Saved Human to DB ***");

        return ResponseEntity.ok().body(savedUsers);
    }



    @Operation(summary = "Update Human")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update Human",
                    content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Users.class))}),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @PutMapping
    public ResponseEntity<Users> update(@Parameter(description = "Human object to be updated") @RequestBody Users users) {

        logger.info("*** Updating Human :: {}", users);
        Users updatedUsers = HumanService.update(users);
        logger.info("*** Updated Human to DB :: {}", users);

        return ResponseEntity.ok().body(updatedUsers);
    }




    @Operation(summary = "Delete the Human by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete the Human",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = String.class))})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@Parameter(description = "Human id to be deleted") @PathVariable Long id) {

        logger.info("*** Deleting Human from DB for Id :: {}", id);
        HumanService.delete(id);
        logger.info("*** Deleted Human from DB for Id :: {}", id);

        return ResponseEntity.ok().body("Deleted successfully...!");
    }
}