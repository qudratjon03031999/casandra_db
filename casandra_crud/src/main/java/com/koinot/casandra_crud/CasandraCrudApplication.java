package com.koinot.casandra_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasandraCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasandraCrudApplication.class, args);
    }

}
